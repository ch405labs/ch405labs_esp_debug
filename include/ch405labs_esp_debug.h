#ifndef CH405LABS_ESP_DEBUG_H
#define CH405LABS_ESP_DEBUG_H

#include <stdio.h>
#include <ctype.h>

#ifdef __cplusplus
extern "C" {
#endif

    // Debuging
#ifdef CONFIG_DEBUG_STACK 
    static const char TAG_STACK[] = CONFIG_TAG_STACK;
#endif //CONFIG_DEBUG_STACK

#ifdef CONFIG_DEBUG_HEAP
    #include "esp_heap_trace.h"
    #define NUM_RECORDS 100
    static heap_trace_record_t trace_record[NUM_RECORDS]; // This buffer must be in internal RAM
#endif //CONFIG_DEBUG_HEAP

void hexdump(void *ptr, int buflen);

#ifdef __cplusplus
    } // extern "C"
#endif

#endif //CH405LABS_ESP_DEBUG_H